import java.util.Scanner; // Allows us to receive inputs and display outputs

public class Hangman {

  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in); // Creates a new scanner

    System.out.print("Enter a 4 character word with no repeating letters: "); // Displays a message
    String wordToGuess = scanner.next().toLowerCase(); // Converts the input into a variable and converts the word to lowercase for case insensitivity

    runGame(wordToGuess); // Calls the rungame method
  }


  public static int isLetterInWord(String word, char c) {
    for (int i = 0; i < 4; i++) {
      if (toUpperCase(word.charAt(i)) == toUpperCase(c)) { // Checks if the inputed letter is in the word
        return i; // If yes, return the "position" of the letter
      }
    }
        return -1; // If not, return -1
  }


  public static char toUpperCase(char c) {
    if (Character.isLowerCase(c)) { // Checks if the letter is lowercase
      return Character.toUpperCase(c); // If yes, convert it to an upper case letter
    }
    return c; // If not, keep it upper case
  }


  public static void printWord(String word, boolean[] lettersGuessed) {
    StringBuilder result = new StringBuilder("Your result is "); // Displays the result so far

      for (int i = 0; i < 4; i++) {
        if (lettersGuessed[i]) { // Checks if the letter was found in the word
          result.append(word.charAt(i)).append(' '); // Adds a space after the found letter
        } else {
          result.append("_ "); // Adds a _ if the letter is missing
      }
    }
    System.out.println(result.toString()); // Displays the result
  }


  public static void runGame(String word) {
    boolean[] lettersGuessed = new boolean[4]; // Creates an array of 4 "slots" using the word inputed before
    int misses = 0; // Creates a variable "misses"
    while (misses < 6) {

      System.out.print("Enter a letter: "); // Outputs the message to input a letter
      Scanner scanner = new Scanner(System.in); // Creates a new scanner

      char guess = toUpperCase(scanner.next().charAt(0)); // Puts the guessed letter into a variable
      int position = isLetterInWord(word, guess); // Calls the method
        if (position != -1) { 
          lettersGuessed[position] = true; // Adds the letter to the result without displaying it
        } else {
          misses++; // Adds a miss if the letter is not in the word
        }
      printWord(word, lettersGuessed); // Calls the method
      boolean wordGuessed = true; // Creates a variable for the result
        for (boolean letterGuessed : lettersGuessed) { // Uses a for each loop to loop over arrays
          if (!letterGuessed) { // Checks if the word is not guessed
            wordGuessed = false; // Turns the variable into a false value
            break; // Used to terminate the loop so that it stops guessing at some point
        }
      }
      if (wordGuessed && misses < 6) { // If the word is guessed under 6 misses
        System.out.println("Yahoooo! You found the word: " + word); // Displays victory message
        break; // Termiates the while loop so that the game stops
      }
    }
      if (misses >= 6) { // If the amount of misses has been reached
        System.out.println("Oops, you lost. The word was: " + word); // Displays the defeat message   
    }      
  }
  }
